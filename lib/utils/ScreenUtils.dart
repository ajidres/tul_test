import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:tultest/styles/colors.dart';

class ScreenUtils{
 static void showToast(String msg){
   // Fluttertoast.showToast(
   //     msg: ,
   //     toastLength: Toast.,
   //     gravity: ToastGravity.CENTER,
   //     backgroundColor: colorAccent,
   //     textColor: Colors.white,
   //     fontSize: 16.0
   // );

   Fluttertoast.showToast(msg: msg, toastLength: Toast.LENGTH_SHORT,
       gravity: ToastGravity.CENTER,
       backgroundColor: colorAccent,
       textColor: Colors.white,
       fontSize: 16.0);
 }
}