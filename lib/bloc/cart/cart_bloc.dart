import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';
import 'package:tultest/domain/Products.dart';

part 'cart_event.dart';

part 'cart_state.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  final CART_STATUS_PENDING = 'pending';
  List<Products> lisProducts = [];
  int cartId = 0;

  CartBloc() : super(CartInitial()) {
    on<CartEvent>((event, emit) {
      /**EVENT FetchCart*/
      if (event is FetchCart) {
        fetchCart();
      }

      /**EVENT RemoveQty*/
      if (event is RemoveQty) {
        removeqty(event.qty, event.productId);
      }

      /**EVENT RemoveProduct*/
      if (event is RemoveProduct) {
        removeProduct(event.productId);
      }

      /**EVENT RemoveCart*/
      if (event is RemoveCart) {
        removeCart();
      }
    });
  }

  Future<void> fetchCart() async {
    await FirebaseFirestore.instance.collection('Carts').where('status', isEqualTo: CART_STATUS_PENDING).get().then((value) async {
      if (value.size > 0) {
        cartId = (value.docs.single as dynamic)['id'];

        await FirebaseFirestore.instance.collection('Product_carts').where('cart_id', isEqualTo: cartId).get().then((value) async {
          List<int> listIds = [];
          lisProducts = [];
          value.docs.forEach((docResults) {
            lisProducts.add(Products(id: (docResults.data() as dynamic)['product_id'], qty: (docResults.data() as dynamic)['quantity']));
            listIds.add((docResults.data() as dynamic)['product_id']);
          });

          await FirebaseFirestore.instance.collection('Products').where('id', whereIn: listIds).get().then((value) async {
            value.docs.forEach((docResults) {
              lisProducts.forEach((element) {
                if (element.id == (docResults.data() as dynamic)['id']) {
                  element.nombre = (docResults.data() as dynamic)['nombre'];
                }

                // lisProducts.add(Products(
                //     id: (docResults.data() as dynamic)['id'],
                //     description: (docResults.data() as dynamic)['description'],
                //     nombre: (docResults.data() as dynamic)['nombre'],
                //     sku: (docResults.data() as dynamic)['sku']));
              });
            });
            emit(CartStateFetchProducts());
          });
        });
      } else {
        emit(CartStateFetchProductsError());
      }
    }).catchError((error, stackTrace) {
      emit(CartStateFetchProductsError());
    });
  }

  Future<void> removeqty(int qty, int idProduct) async {
    await FirebaseFirestore.instance
        .collection('Product_carts')
        .where('cart_id', isEqualTo: cartId)
        .where('product_id', isEqualTo: idProduct)
        .get()
        .then((value) {
      FirebaseFirestore.instance
          .collection('Product_carts')
          .doc(value.docs.single.id)
          .update({'quantity': qty}).then((value) => emit(CartStateUpdateQtyProducts()));
    });
  }

  Future<void> removeProduct(int idProduct) async {
    await FirebaseFirestore.instance
        .collection('Product_carts')
        .where('cart_id', isEqualTo: cartId)
        .where('product_id', isEqualTo: idProduct)
        .get()
        .then((value) {
      FirebaseFirestore.instance
          .collection('Product_carts')
          .doc(value.docs.single.id)
          .delete()
          .then((value) {

        lisProducts.removeWhere((element) => element.id==idProduct);
        emit(CartStateDeleteProducts());
      });
    });
  }

  void removeCart() async{

    await FirebaseFirestore.instance
        .collection('Product_carts')
        .where('cart_id', isEqualTo: cartId)
        .get()
        .then((value) {
      FirebaseFirestore.instance
          .collection('Product_carts')
          .doc(value.docs.single.id)
          .delete();

    });

    await FirebaseFirestore.instance
        .collection('Carts')
        .where('id', isEqualTo: cartId)
        .get()
        .then((value) {
      FirebaseFirestore.instance
          .collection('Carts')
          .doc(value.docs.single.id)
          .delete().then((value) => emit(CartStateRemoveCart()));

    });
  }
}
