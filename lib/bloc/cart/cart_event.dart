part of 'cart_bloc.dart';

@immutable
abstract class CartEvent {}

class FetchCart extends CartEvent {}

class RemoveCart extends CartEvent {}

class RemoveProduct extends CartEvent {
  final productId;
  RemoveProduct(this.productId);
}

class RemoveQty extends CartEvent {
  final int qty;
  final productId;
  RemoveQty(this.qty, this.productId);
}
