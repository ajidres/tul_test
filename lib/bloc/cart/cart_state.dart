part of 'cart_bloc.dart';

@immutable
abstract class CartState {}

class CartInitial extends CartState {}

class CartStateFetchProducts extends CartState {}

class CartStateFetchProductsError extends CartState {}

class CartStateUpdateQtyProducts extends CartState {}

class CartStateDeleteProducts extends CartState {}

class CartStateRemoveCart extends CartState {}