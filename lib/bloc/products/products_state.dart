part of 'products_bloc.dart';

@immutable
abstract class ProductsState {}

class ProductsStateInitial extends ProductsState {}

class ProductsStateFetchProducts extends ProductsState {}

class ProductsStateFetchProductsError extends ProductsState {}

class ProductsStateCreateCart extends ProductsState {}
class ProductsStateUpdateCart extends ProductsState {}