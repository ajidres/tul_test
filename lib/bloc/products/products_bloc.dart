import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';
import 'package:tultest/domain/Products.dart';

part 'products_event.dart';

part 'products_state.dart';

class ProductsBloc extends Bloc<ProductsEvent, ProductsState> {
  final CART_STATUS_PENDING = 'pending';
  final CART_STATUS_COMPLETE = 'complete';
  List<Products> lisProducts = [];

  ProductsBloc() : super(ProductsStateInitial()) {
    on<ProductsEvent>((event, emit) {
      /**EVENT FetchProducts*/
      if (event is FetchProducts) {
        fetchProducts();
      }

      /**EVENT AddProducts*/
      if (event is AddProducts) {
        addProducts(event.qty, event.productId);
      }
    });
  }

  Future<void> fetchProducts() async {
    await FirebaseFirestore.instance.collection('Products').orderBy('nombre').get().then((value) {
      value.docs.forEach((docResults) {
        lisProducts.add(Products(
            id: (docResults.data() as dynamic)['id'],
            description: (docResults.data() as dynamic)['description'],
            nombre: (docResults.data() as dynamic)['nombre'],
            sku: (docResults.data() as dynamic)['sku']));
      });

      emit(ProductsStateFetchProducts());
    }).catchError((error, stackTrace) {
      emit(ProductsStateFetchProductsError());
    });
  }

  void addProducts(int qty, int idProduct) async {
    var cartId;

    await FirebaseFirestore.instance.collection('Carts').where('status', isEqualTo: CART_STATUS_PENDING).get().then((value) {
      if (value.size > 0) {
        print('');
        cartId = (value.docs.single as dynamic)['id'];
        updateCart(cartId, qty, idProduct);
      } else {
        createNewCart(qty, idProduct);
      }
    });
  }

  Future<void> createNewCart(int qty, int idProduct) async {
    print('');
    await FirebaseFirestore.instance.collection('Carts').orderBy('id').get().then((value) {
      var cartId = value.size + 1;

      FirebaseFirestore.instance.collection("Carts").add({
        'id': cartId,
        'status': CART_STATUS_PENDING,
      }).then((value) {
        FirebaseFirestore.instance
            .collection("Product_carts")
            .add({'product_id': idProduct, 'cart_id': cartId, 'quantity': qty}).then((value) {
          emit(ProductsStateCreateCart());
        });
      });
    });
  }

  void updateCart(int cartId, int qty, int idProduct) async {
    await FirebaseFirestore.instance
        .collection('Product_carts')
        .where('cart_id', isEqualTo: cartId)
        .where('product_id', isEqualTo: idProduct)
        .get()
        .then((value) {
      if (value.size > 0) {
        FirebaseFirestore.instance
            .collection('Product_carts')
            .doc(value.docs.single.id)
            .update({'quantity': (value.docs.single as dynamic)['quantity']+qty}).then((value) => emit(ProductsStateUpdateCart()));
      } else {
        FirebaseFirestore.instance
            .collection("Product_carts")
            .add({'product_id': idProduct, 'cart_id': cartId, 'quantity': qty}).then((value) {
          emit(ProductsStateCreateCart());
        });
      }
    });
  }
}
