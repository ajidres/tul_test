part of 'products_bloc.dart';

@immutable
abstract class ProductsEvent {}

class FetchProducts extends ProductsEvent {}


class AddProducts extends ProductsEvent {
  final int qty;
  final productId;
  AddProducts(this.qty, this.productId);
}