import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tultest/ui/productspage.dart';

import 'core/injection_manager.dart';
import 'ui/component/loading_full_component.dart';
import 'ui/component/page_base_component.dart';


void main() {
  InjectionManager.setupInjections();
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Tul Test',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: FutureBuilder(
        future: Firebase.initializeApp(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            //todo
            // return SomethingWentWrong();
            print('');
          }

          if (snapshot.connectionState == ConnectionState.done) {
            return const ProductPage();
          }


          return _loadingPage();
        },
      )
    );
  }
}



Widget _loadingPage() {
  return PageBaseComponent(
    body: LoadingFullComponent(),
  );
}