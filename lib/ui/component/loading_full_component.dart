// Flutter imports:

// Flutter imports:
import 'package:flutter/material.dart';

import 'loading_component.dart';


// Project imports:

class LoadingFullComponent extends StatelessWidget {

  const LoadingFullComponent({
    Key? key,
  }): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.transparent),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Center(

        child: LoadingComponent()

      ),
    );
  }
}
