
import 'package:flutter/material.dart';
import 'package:tultest/styles/colors.dart';

class LoadingComponent extends StatelessWidget {

  const LoadingComponent({
    Key? key,
  }): super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircularProgressIndicator(
      value: null,
      strokeWidth: 2.0,
      color: colorAccent,
    );
  }
}
