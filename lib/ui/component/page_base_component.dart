// Flutter imports:

// Flutter imports:
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:tultest/core/context_manager.dart';
import 'package:tultest/core/injection_manager.dart';

// Project imports:

class PageBaseComponent extends StatelessWidget {
  final PreferredSizeWidget? appBar;
  final Widget body;


  const PageBaseComponent({Key? key, required this.body, this.appBar}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    getIt<ContextManager>().screenSize = Size(MediaQuery.of(context).size.width, MediaQuery.of(context).size.height-MediaQuery.of(context).padding.top);


    return Scaffold(
      backgroundColor: Colors.white,
      appBar: appBar,
      body: SafeArea(child: body),
    );

  }
}
