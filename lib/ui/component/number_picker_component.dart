import 'dart:async';

import 'package:flutter/material.dart';

class CustomNumberPicker<T extends num> extends StatefulWidget {


  final Function(T) onValue;
  final T maxValue;
  final T minValue;
  final T initialValue;
  final T step;

  ///default vale true
  final bool enable;

  CustomNumberPicker(
      {Key? key,
        required this.onValue,
        required this.initialValue,
        required this.maxValue,
        required this.minValue,
        required this.step,
        this.enable = true})
      : assert(initialValue.runtimeType != String),
        assert(maxValue.runtimeType == initialValue.runtimeType),
        assert(minValue.runtimeType == initialValue.runtimeType),
        super(key: key);

  @override
  State<StatefulWidget> createState() {
    return CustomNumberPickerState();
  }
}

class CustomNumberPickerState<T extends num> extends State<CustomNumberPicker<T>> {
  late num _initialValue;
  late num _maxValue;
  late num _minValue;
  late num _step;
  Timer? _timer;

  @override
  void initState() {
    super.initState();
    _initialValue = widget.initialValue;
    _maxValue = widget.maxValue;
    _minValue = widget.minValue;
    _step = widget.step;
  }

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: !widget.enable,
      child: Card(
        shadowColor: Colors.transparent,
        elevation: 0.0,
        semanticContainer: true,
        color: Colors.transparent,
        shape:
            RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(0.0)),
                side: BorderSide(width: 1.0, color: Colors.grey)),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: minus,
              onTapDown: (details) {
                onLongPress(DoAction.MINUS);
              },
              onTapUp: (details) {
                _timer?.cancel();
              },
              onTapCancel: () {
                _timer?.cancel();
              },
              child:
                  Padding(
                    padding:
                    EdgeInsets.only(left: 6, right: 6, bottom: 6, top: 6),
                    child: Icon(Icons.remove,
                      size: 15,
                      color: Colors.black,
                    ),
                  ),
            ),
            Container(
              padding: EdgeInsets.only(left: 16, right: 16),
              // width: _textSize(widget.valueTextStyle ?? TextStyle(fontSize: 14))
              //     .width,
              child: Text(
                '$_initialValue',
                style: TextStyle(fontSize: 14),
                textAlign: TextAlign.center,
              ),
            ),
            GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: add,
              onTapDown: (details) {
                onLongPress(DoAction.ADD);
              },
              onTapUp: (details) {
                _timer?.cancel();
              },
              onTapCancel: () {
                _timer?.cancel();
              },
              child:
                  Padding(
                    padding:
                    EdgeInsets.only(left: 6, right: 6, bottom: 6, top: 6),
                    child: Icon(Icons.add,
                      size: 15,
                      color: Colors.black,
                    ),
                  ),
            )
          ],
        ),
      ),
    );
  }

  Size _textSize(TextStyle style) {
    final textPainter = TextPainter(
        text: TextSpan(text: _maxValue.toString(), style: style),
        maxLines: 1,
        textDirection: TextDirection.ltr)
      ..layout(
          minWidth: 0, maxWidth: _maxValue.toString().length * style.fontSize!);
    return textPainter.size;
  }

  void minus() {
    if (canDoAction(DoAction.MINUS)) {
      setState(() {
        _initialValue -= _step;
      });
    }
    widget.onValue(_initialValue as T);
  }

  void add() {
    if (canDoAction(DoAction.ADD)) {
      setState(() {
        _initialValue += _step;
      });
    }
    widget.onValue(_initialValue as T);
  }

  void onLongPress(DoAction action) {
    var timer = Timer.periodic(Duration(milliseconds: 300), (t) {
      action == DoAction.MINUS ? minus() : add();
    });
    setState(() {
      _timer = timer;
    });
  }

  bool canDoAction(DoAction action) {
    if (action == DoAction.MINUS) {
      return _initialValue - _step >= _minValue;
    }
    if (action == DoAction.ADD) {
      return _initialValue + _step <= _maxValue;
    }
    return false;
  }
}

enum DoAction { MINUS, ADD }
