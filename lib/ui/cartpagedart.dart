import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tultest/bloc/cart/cart_bloc.dart';
import 'package:tultest/styles/colors.dart';
import 'package:tultest/utils/ScreenUtils.dart';

import 'component/loading_full_component.dart';
import 'component/number_picker_component.dart';
import 'component/page_base_component.dart';

class CartPage extends StatefulWidget {
  const CartPage({Key? key}) : super(key: key);

  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  final CartBloc _cartBloc = CartBloc();

  @override
  void initState() {
    super.initState();
    _cartBloc.add(FetchCart());
  }
  
  @override
  Widget build(BuildContext context) {
    return PageBaseComponent(
        appBar: AppBar(
          title: Text('Tul Test Cart'),
          actions: [
            IconButton(
              icon: Icon(Icons.remove_shopping_cart),
              iconSize: 20,
              color: Colors.white,
              onPressed: () {
                _cartBloc.add(RemoveCart());
              },
            )
          ],
        ),
        body: BlocProvider<CartBloc>(
            lazy: false,
            create: (context) => _cartBloc,
            child:
            BlocConsumer<CartBloc, CartState>(
                listener: (context, state) {
                  if (state is CartStateUpdateQtyProducts) ScreenUtils.showToast('Carrito actualizado exitosamente!!');
                  if (state is CartStateDeleteProducts) ScreenUtils.showToast('Producto eliminado del carrito exitosamente!!');
                  if (state is CartStateRemoveCart) ScreenUtils.showToast('Carrito eliminado exitosamente!!');

                },
                builder: (context, state) {
                  return Stack(
                    children: [
                      _body(state),
                      if (state is CartInitial)
                        LoadingFullComponent()
                    ],
                  );
                })));
  }

  Widget _body (CartState state) {

    if(state is CartStateRemoveCart){
      return Container();
    }

    if (_cartBloc.lisProducts.isNotEmpty) {
      return Padding(
        padding: const EdgeInsets.only(left: 32, right: 32),
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: _cartBloc.lisProducts.length,
          shrinkWrap: true,
          itemBuilder: (contextListView, index) {
            return Padding(
                padding: EdgeInsets.only(bottom: 16, top: 16),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Text(_cartBloc.lisProducts[index].nombre!),
                    ),
                    CustomNumberPicker(
                      initialValue: _cartBloc.lisProducts[index].qty,
                      maxValue: 50,
                      minValue: 1,
                      step: 1,
                      onValue: (value) {
                        if((value as int)==1){
                          _cartBloc.add(RemoveProduct(_cartBloc.lisProducts[index].id));
                        }else{
                          _cartBloc.add(RemoveQty(value, _cartBloc.lisProducts[index].id));
                        }

                      },
                    ),
                    IconButton(
                      icon: Icon(Icons.remove_shopping_cart),
                      iconSize: 20,
                      color: colorAccent,
                      onPressed: () {
                        _cartBloc.add(RemoveProduct(_cartBloc.lisProducts[index].id));
                      },
                    )
                  ],
                ));
          },
        ),
      );
    }

    if (state is CartStateFetchProductsError) {
      return Container(
        child: Center(
          child: Text('Error'),
        ),
      );
    }

    return Container();
  }
}
