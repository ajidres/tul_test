import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tultest/bloc/products/products_bloc.dart';
import 'package:tultest/core/context_manager.dart';
import 'package:tultest/core/injection_manager.dart';
import 'package:tultest/styles/colors.dart';
import 'package:tultest/ui/cartpagedart.dart';
import 'package:tultest/ui/component/page_base_component.dart';
import 'package:tultest/utils/ScreenUtils.dart';

import 'component/loading_full_component.dart';
import 'component/number_picker_component.dart';

class ProductPage extends StatefulWidget {
  const ProductPage({Key? key}) : super(key: key);

  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  final ProductsBloc _productsBloc = ProductsBloc();


  @override
  void initState() {
    super.initState();
    getIt<ContextManager>().context = context;
    _productsBloc.add(FetchProducts());
  }

  @override
  Widget build(BuildContext context) {
    return PageBaseComponent(
        appBar: AppBar(
          title: Text('Tul Test'),
          actions: [
            IconButton(
              icon: Icon(Icons.shopping_cart),
              iconSize: 20,
              color: Colors.white,
              onPressed: () {

                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => CartPage(), fullscreenDialog: true)
                );

              },
            )
          ],
        ),
        body: BlocProvider<ProductsBloc>(
            lazy: false,
            create: (context) => _productsBloc,
            child:
                BlocConsumer<ProductsBloc, ProductsState>(
                    listener: (context, state) {
                      if (state is ProductsStateCreateCart) ScreenUtils.showToast('Carrito creado exitosamente!!');
                      if (state is ProductsStateUpdateCart) ScreenUtils.showToast('Carrito actualizado exitosamente!!');

                    },
                    builder: (context, state) {
                      return Stack(
                        children: [
                          _body(state),
                          if (state is ProductsStateInitial)
                            LoadingFullComponent()
                        ],
                      );
                    })));
  }

  Widget _body (ProductsState state) {

    if (_productsBloc.lisProducts.isNotEmpty) {
      return Padding(
        padding: const EdgeInsets.only(left: 32, right: 32),
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: _productsBloc.lisProducts.length,
          shrinkWrap: true,
          itemBuilder: (contextListView, index) {
            return Padding(
                padding: EdgeInsets.only(bottom: 16, top: 16),
                child: Card(
                  child: ExpansionTile(
                    initiallyExpanded: false,
                    backgroundColor: Colors.white,
                    title: Row(
                      children: [
                        Expanded(
                          flex: 1,
                          child: Text(_productsBloc.lisProducts[index].nombre!),
                        ),
                        CustomNumberPicker(
                          initialValue: _productsBloc.lisProducts[index].qty,
                          maxValue: 50,
                          minValue: 1,
                          step: 1,
                          onValue: (value) {
                            _productsBloc.lisProducts[index].qty = value! as int;
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.add_shopping_cart),
                          iconSize: 20,
                          color: colorAccent,
                          onPressed: () {

                            _productsBloc.add(AddProducts(_productsBloc.lisProducts[index].qty,_productsBloc.lisProducts[index].id));

                            setState(() {
                              _productsBloc.lisProducts[index].qty=1;
                            });

                          },
                        )
                      ],
                    ),
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8),
                        child: Text(
                          _productsBloc.lisProducts[index].description!,
                        ),
                      )
                    ],
                  ),
                ));
          },
        ),
      );
    }

    if (state is ProductsStateFetchProductsError) {
      return Container(
        child: Center(
          child: Text('Error'),
        ),
      );
    }

    return Container();
  }
}
