import 'dart:convert';

Products productsFromJson(String str) => Products.fromJson(json.decode(str));

String productsToJson(Products data) => json.encode(data.toJson());

class Products {
  Products({
    this.description,
    this.id,
    this.nombre,
    this.sku,
    this.qty=1
  });

  String? description;
  int? id;
  int qty;
  String? nombre;
  String? sku;

  factory Products.fromJson(Map<String, dynamic> json) => Products(
    description: json["description"],
    id: json["id"],
    nombre: json["nombre"],
    sku: json["sku"],
  );

  Map<String, dynamic> toJson() => {
    "description": description,
    "id": id,
    "nombre": nombre,
    "sku": sku,
  };
}

