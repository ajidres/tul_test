import 'dart:convert';

Products productsFromJson(String str) => Products.fromJson(json.decode(str));

String productsToJson(Products data) => json.encode(data.toJson());

class Products {
  Products({
    this.id,
    this.nombre,
    this.qty=1
  });


  int? id;
  int qty;
  String? nombre;


  factory Products.fromJson(Map<String, dynamic> json) => Products(

    id: json["id"],
    nombre: json["nombre"],

  );

  Map<String, dynamic> toJson() => {

    "id": id,
    "nombre": nombre,

  };
}

