// Package imports:

// Package imports:
import 'package:flutter_secure_storage/flutter_secure_storage.dart';


class PreferenceManager {

  final FlutterSecureStorage flutterSecureStorage = FlutterSecureStorage();

  Future<void> saveData(String key, String? data) async {
    await flutterSecureStorage.write(key: key, value: data);
  }

  Future<void> clearAllData() async {
    await flutterSecureStorage.deleteAll();
  }


  Future<String?> readStringData(String key) async {
    return await flutterSecureStorage.read(key: key);
  }

  Future<int> readIntData(String key) async {
    var data = await flutterSecureStorage.read(key: key);

    if (data == null) {
      return 0;
    } else {
      return int.parse(data);
    }
  }





}