// Package imports:

// Package imports:
import 'package:get_it/get_it.dart';
import 'package:tultest/data/prefrences_manager.dart';

import 'context_manager.dart';


final getIt = GetIt.instance;

class InjectionManager {
  static void setupInjections() {
    // getIt.registerSingleton<RepositoryManager>(RepositoryManager());
    getIt.registerSingleton<ContextManager>(ContextManager());
    getIt.registerSingleton(PreferenceManager());
  }
}
